package org.usfirst.frc.team5926.robot.commands;

import org.usfirst.frc.team5926.robot.Robot;

import edu.wpi.first.wpilibj.command.Command;

/**
 * Auton Command for just crossing the Base Line... LAST RESORT...
 * Sadly we have no encoders so we just dead reckon...
 */

public class AutoCrossBaseLine_Command extends Command {
	
	// Create time variables
	protected long forwardTime;
	protected long forwardStopTime;
	protected long startTime;
    
	public AutoCrossBaseLine_Command() {
		
		// Set time to action(s)
		forwardTime = 2000;
		
        requires(Robot.DriveTrain);
        requires(Robot.FrameLight);
	}
	
	@Override
	protected void initialize() {
		
		// Get the current time when the command starts executing
		startTime = System.currentTimeMillis();
		
		// Calculate the time when action(s) will be executed
		forwardStopTime = startTime + forwardTime;
	}

	// Called repeatedly when this Command is scheduled to run
	@Override
	protected void execute() {
		
		if (forwardStopTime >= System.currentTimeMillis()) {
			
			// Drive forward
			Robot.DriveTrain.autoDrive(-0.5, -0.5);
		}
		else {
			
			// Stop Driving
			Robot.DriveTrain.autoDrive(-0.0, -0.0);
			
			// Light up frame to rejoice that we moved (is that any surprise? 8| )... 
			Robot.FrameLight.frameGlow(true);
		}	
		
	}

	// Make this return true when this Command no longer needs to run execute()
	@Override
	protected boolean isFinished() {
		return false;
	}

	// Called once after isFinished returns true
	@Override
	protected void end() {
		// Well that embarrassment is over...
	}

	// Called when another command which requires one or more of the same
	// subsystems is scheduled to run
	@Override
	protected void interrupted() {
		end();
	}

}
