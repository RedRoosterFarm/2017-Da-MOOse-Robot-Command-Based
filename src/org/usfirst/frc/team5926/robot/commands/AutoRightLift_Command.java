package org.usfirst.frc.team5926.robot.commands;

import org.usfirst.frc.team5926.robot.Robot;

import edu.wpi.first.wpilibj.command.Command;

/*
 * Auton Command for placing a Gear on the right lift peg...
 * This is the most complex auto we have...
 * 33.333333333333333333333333333333333333333333%* Accuracy  
 */

public class AutoRightLift_Command extends Command {
	
	// Create time variables...
	protected long clawOpenTime;
	protected long forwardTime;
	protected long preClawTime;
	protected long forwardStopTime;
	protected long preLeftTime;
	protected long leftStartTime;
	protected long leftTime;
	protected long leftStopTime;
	protected long startTime;
	protected long leftExtraTime;
	protected long leftExtraStopTime;
	// there is a lot...
	
	public AutoRightLift_Command() {
		
		// Set time to action(s)
		preClawTime = 1000;
		preLeftTime = 1000;
		forwardTime = 1650;
		leftTime = 2050;
		leftExtraTime = 400;
		
        requires(Robot.DriveTrain);
        requires(Robot.GearClaw);
        requires(Robot.FrameLight);
	}
	
	@Override
	protected void initialize() {
		
		// Get the current time when the command starts executing
		startTime = System.currentTimeMillis();
		
		// Calculate the time when action(s) will be executed
		forwardStopTime = startTime + forwardTime;
		leftStartTime = forwardStopTime + preLeftTime;
		leftStopTime = leftStartTime + leftTime;
		leftExtraStopTime = leftStopTime + leftExtraTime;
		clawOpenTime = leftStopTime + preClawTime;
	}

	// Called repeatedly when this Command is scheduled to run
	@Override
	protected void execute() {
		
		if (forwardStopTime >= System.currentTimeMillis()) {
			
			// Drive forward
			Robot.DriveTrain.autoDrive(-0.35, -0.39);
		}
		else if (forwardStopTime <= System.currentTimeMillis() &&  System.currentTimeMillis() <= leftStartTime) {
			
			// Pause for a second
			Robot.DriveTrain.autoDrive(-0.0, -0.0);
		}
		else if (leftStartTime <= System.currentTimeMillis() && System.currentTimeMillis() <= leftStopTime) {
			
			// Turn Left... What would happen if you turned right? Your life as a programmer would end right there... Bad DW reference
			Robot.DriveTrain.autoDrive(-0.40, 0.31);
		}
		else if (leftStopTime <= System.currentTimeMillis() && System.currentTimeMillis() <= leftExtraStopTime) {
			
			// Keep turning left 8P
			Robot.DriveTrain.autoDrive(-0.35, -0.39);
		}
		else {
			
			// Now we can finally stop... 
			Robot.DriveTrain.autoDrive(-0.0, -0.0);
		}
		
		if (System.currentTimeMillis() >= clawOpenTime) {
			
			// Open the Gear Claw
			Robot.GearClaw.clawOpen(true);
			
			// Light up frame to signal Human Player to retrieve the gear... If we didn't already drop it on the floor...
			Robot.FrameLight.frameGlow(true);
		}
		
		
		
	}

	// Make this return true when this Command no longer needs to run execute()
	@Override
	protected boolean isFinished() {
		return false;
	}

	// Called once after isFinished returns true
	@Override
	protected void end() {
		
		// Close the Gear Claw
		Robot.GearClaw.clawOpen(false);
	}

	// Called when another command which requires one or more of the same
	// subsystems is scheduled to run
	@Override
	protected void interrupted() {
		end();
	}

}
// * Give or take a few percent...