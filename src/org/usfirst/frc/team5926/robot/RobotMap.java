package org.usfirst.frc.team5926.robot;


/**
 * The RobotMap is a mapping from the ports sensors and actuators are wired into
 * to a variable name. This provides flexibility changing wiring, makes checking
 * the wiring easier and significantly reduces the number of magic numbers
 * floating around.
 */
public class RobotMap {
	
	// CAN Device Setup
	public static class CAN {
		public static int frontLeftDrive = 0;
		public static int rearLeftDrive = 1;
		public static int frontRightDrive = 2;
		public static int rearRightDrive = 3;
		public static int robotLift = 4;
		//public static int ballIntake = 5;
		//public static int shooterFlywheel = 6;
	}
	
	// PCM Circuit Setup
	public static class PCM {
		public static int liftCircuit = 1;
		public static int clawCircuit = 0;
		public static int frameGlowCircuit = 2;
		public static int ringLightCircuit = 3;
	}
	
}
