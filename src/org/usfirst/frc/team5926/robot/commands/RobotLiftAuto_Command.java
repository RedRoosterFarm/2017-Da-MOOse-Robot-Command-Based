package org.usfirst.frc.team5926.robot.commands;

import org.usfirst.frc.team5926.robot.Robot;

import edu.wpi.first.wpilibj.command.Command;

/*
 * "Smart" Rope Climber command class
 */

public class RobotLiftAuto_Command extends Command {
	
	// Create time variables
	protected long startTime;
	protected long fastTimeDelay;
	protected long fastStartTime;
	
	public RobotLiftAuto_Command() {

		// Set time to action(s)
		fastTimeDelay = 1500;
		
		requires(Robot.RobotLift);	}

	@Override
	protected void initialize() {
		
		// Get the current time when the command starts executing
		startTime = System.currentTimeMillis();
		
		// Calculate the time when action(s) will be executed
		fastStartTime = startTime + fastTimeDelay;
	}

	// Called repeatedly when this Command is scheduled to run
	@Override
	protected void execute() {
		
		if (System.currentTimeMillis() <= fastStartTime) {
			
			// Climb slow to grip rope
			Robot.RobotLift.ropeClimberSlow();
		}
		else {
			
			// Climb faster once rope is gripped
			Robot.RobotLift.ropeClimberFast();
		}
	}

	// Make this return true when this Command no longer needs to run execute()
	@Override
	protected boolean isFinished() {
		return false;
	}

	// Called once after isFinished returns true
	@Override
	protected void end() {
		
		// Stop the Rope Climber
		Robot.RobotLift.ropeClimberOff();
	}

	// Called when another command which requires one or more of the same
	// subsystems is scheduled to run
	@Override
	protected void interrupted() {
		end();
	}
}
