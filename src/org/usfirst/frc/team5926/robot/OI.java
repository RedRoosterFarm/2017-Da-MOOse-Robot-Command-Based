package org.usfirst.frc.team5926.robot;

import org.usfirst.frc.team254.robot.utils.Latch;
import org.usfirst.frc.team5926.robot.commands.ArcadeDriveSlow_Command;
import org.usfirst.frc.team5926.robot.commands.GearClawLift_Command;
import org.usfirst.frc.team5926.robot.commands.GearClaw_Command;
import org.usfirst.frc.team5926.robot.commands.RobotLiftAuto_Command;
import org.usfirst.frc.team5926.robot.commands.RobotLift_Command;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.Button;
import edu.wpi.first.wpilibj.buttons.JoystickButton;

/**
 * This class is the glue that binds the controls on the physical operator
 * interface to the commands and command groups that allow control of the robot.
 */
public class OI {
	/*// CREATING BUTTONS
	// One type of button is a joystick button which is any button on a
	//// joystick.
	// You create one by telling it which joystick it's on and which button
	// number it is.
	// Joystick stick = new Joystick(port);
	// Button button = new JoystickButton(stick, buttonNumber);

	// There are a few additional built in buttons you can use. Additionally,
	// by subclassing Button you can create custom triggers and bind those to
	// commands the same as any other Button.

	//// TRIGGERING COMMANDS WITH BUTTONS
	// Once you have a button, it's trivial to bind it to a button in one of
	// three ways:

	// Start the command when the button is pressed and let it run the command
	// until it is finished as determined by it's isFinished method.
	// button.whenPressed(new ExampleCommand());

	// Run the command while the button is being held down and interrupt it once
	// the button is released.
	// button.whileHeld(new ExampleCommand());

	// Start the command when the button is released and let it run the command
	// until it is finished as determined by it's isFinished method.
	// button.whenReleased(new ExampleCommand()); */
	
	Latch brakeLatch = new Latch();
	
	// USB HID
	Joystick driveStick = new Joystick(0),
			buttonBoard = new Joystick(1);
	
	// OC Buttons
	Button buttonGearClawLift = new JoystickButton(buttonBoard, 1),
			buttonGearClawLift2 = new JoystickButton(driveStick, 3),
			buttonGearClaw2 = new JoystickButton(driveStick, 4),
			buttonGearClaw = new JoystickButton(buttonBoard, 3),
			buttonRobotLift = new JoystickButton(buttonBoard, 4),
			buttonRobotSlow = new JoystickButton(driveStick, 2),
			buttonRobotLiftAuto = new JoystickButton(buttonBoard, 6);
	
	
	
	public OI() {
		
		// Gear Handler
		buttonGearClaw.whileHeld(new GearClaw_Command());
		buttonGearClawLift.whileHeld(new GearClawLift_Command());
		
		// Precision Driving Mode 
		buttonRobotSlow.whileHeld(new ArcadeDriveSlow_Command());
		
		// Rope Climber
		buttonRobotLift.whileHeld(new RobotLift_Command());
		buttonRobotLiftAuto.whileHeld(new RobotLiftAuto_Command());
		
		// Talon Brake Mode
		if (brakeLatch.update(driveStick.getRawButton(1))) {
			Robot.DriveTrain.driveBrakeMode(false);
		}
		else {
			Robot.DriveTrain.driveBrakeMode(true);
		}
		
	}
	
	// Driver's Joystick
	public Joystick getDriverJoystick() {
		return driveStick;
	}
	
}
