package org.usfirst.frc.team5926.robot.subsystems;

import org.usfirst.frc.team5926.robot.RobotMap;

import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.command.Subsystem;

/* 
 * The FrameLight_Subsystem class contains all the to use the Frame Lights  
 */

public class FrameLight_Subsystem extends Subsystem {
	
	// Declare the PCM Circuit for the Under Glow
	Solenoid frameGlowSol = new Solenoid(RobotMap.PCM.frameGlowCircuit);
	
	@Override
	protected void initDefaultCommand() {
	}
	
	// Under Glow Function
	public void frameGlow(boolean onButton) {			
		if (onButton == true) {
			frameGlowSol.set(true);
		}
		else {
			frameGlowSol.set(false);
		}
	}
	
}
