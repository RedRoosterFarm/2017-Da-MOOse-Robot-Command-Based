package org.usfirst.frc.team5926.robot.subsystems;

import org.usfirst.frc.team5926.robot.RobotMap;

import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/* 
 * The GearClawLift_Subsystem class contains all the to use the Gear Claw Lift   
 */

public class GearClawLift_Subsystem extends Subsystem {
	
	// Declare the PCM Circuit for the Gear Lift
	Solenoid liftSol = new Solenoid(RobotMap.PCM.liftCircuit);
	
	@Override
	protected void initDefaultCommand() {
	}
	
	// Gear Claw Lift Function
	public void clawLift(boolean onButton) {			
		if (onButton == true) {
			
			// Lower Gear Claw Lift
			liftSol.set(true);
			
			// Send the state of the Gear Claw Lift to the SmartDashboard
			SmartDashboard.putBoolean("clawLift", true);
		}
		else {
			
			// Raise Gear Claw Lift
			liftSol.set(false);
			
			// Send the state of the Gear Claw Lift to the SmartDashboard
			SmartDashboard.putBoolean("clawLift", false);
		}
	}
	
}
