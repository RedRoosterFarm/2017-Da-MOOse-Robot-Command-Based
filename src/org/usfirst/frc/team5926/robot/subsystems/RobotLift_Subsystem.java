package org.usfirst.frc.team5926.robot.subsystems;

import org.usfirst.frc.team5926.robot.RobotMap;

import com.ctre.CANTalon;

import edu.wpi.first.wpilibj.command.Subsystem;

/* 
 * The RobotLift_Subsystem class contains all the to use the Rope Climber
 */

public class RobotLift_Subsystem extends Subsystem {
	
	// Declare the CANTalon for the Rope Climber
	public static final CANTalon robotLift = new CANTalon(RobotMap.CAN.robotLift);
	
	@Override
	protected void initDefaultCommand() {
	}
	
	// High Speed Mode
	public void ropeClimberFast() {
		robotLift.set(-0.95);
	}
	
	// Low Speed Mode
	public void ropeClimberSlow() {
		robotLift.set(-0.65);
	}
	
	// Stop the Rope Climber
	public void ropeClimberOff() {
		robotLift.set(0.0);
	}
}
